<? /*

  Simplate
  by Phillip Ridlen
  
  Super-simple PHP templating engine 
  http://github.com/philtr/simplate

*/ ?>


<?

class Simplate {

  function __construct($simplate) { 
    if(!file_exists("./".$simplate."/layout.smplt.html")) {
      die("SIMPLATE: Simplate not found!");
    } else {
     $this->simplate = $simplate;
     $this->vars = array();
   }
  }

  // public
  
  public $vars;
  
  public function render() {
    $vars = $this->vars;
    include($this->simplate."/layout.smplt.html");
  }
  
  //private
  
  private $simplate;
  
  private function render_sub($sub_name) {
    $vars = $this->vars;
    include($this->simplate."/".$sub_name.".smplt.html");
  }
  
}

?>
